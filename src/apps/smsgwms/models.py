from django.db import models

__author__ = "Andrey Lukyanov"
__email__ = "laandrey@gmail.com"
__all__ = ['SMSGWLog']



class SMSGWLogManager(models.Manager):
    def log(self, handler_name, method, request, response):
        self.create(handler_name=handler_name, method=method,
                    request=request, response=response)


class SMSGWLog(models.Model):
    ts = models.DateTimeField(auto_now_add=True, blank=False, null=False)
    handler_name = models.CharField(max_length=20, blank=False, null=False)
    method = models.CharField(max_length=20, blank=False, null=False)
    request = models.TextField(blank=False, null=False)
    response = models.TextField(blank=False, null=False)

    objects = SMSGWLogManager()

    class Meta:
        ordering = ('-ts', 'handler_name', 'method')
        verbose_name = 'SMS-gateway log'

    def __str__(self):
        return '<SMSGWLog: ts={0}, handler_name={1}, method={2}, ...>'.format(
            self.ts.isoformat(sep=' '), self.handler_name, self.method)
