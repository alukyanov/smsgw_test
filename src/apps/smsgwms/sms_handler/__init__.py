__author__ = "Andrey Lukyanov"
__email__ = "laandrey@gmail.com"


def get_handler(handler_name):
    """Returns SMSGW handler class
    :param handler_name: SMSGW name
    :return:
    """
    module = __import__(handler_name, globals=globals())
    return module.SMSGWClient


