import json
import urllib
import urllib2


__author__ = "Andrey Lukyanov"
__email__ = "laandrey@gmail.com"
__all__ = ['SMSHandler']


class BaseException(Exception):
    def __init__(self, code, message):
        self.code = code
        self.message = message


class SMSHandler(object):
    handler_name = None
    params = {}
    methods = {}
    headers = {}
    response_format = 'PLAIN-TEXT'
    encoding = 'utf-8'
    logger = None

    def _call(self, method, params):
        """Request GW API method
        :param url:
        :param method:
        :return:
        """
        if method not in self.methods:
            raise self.MethodNotFound(
                'SMSGWAPI:E:10002',
                'Method \'{0}\' not found.'.format(method))

        url, http_method = self.methods[method]
        self.params.update(params)

        if http_method in ('POST', 'PUT', 'DELETE'):
            req = urllib2.Request(url, params, self.headers)
        elif http_method == 'GET':
            url += '?{0}'.format(urllib.urlencode(self.params))
            req = urllib2.Request(url, None, self.headers)
        else:
            raise self.HttpMethodNotAllowed(
                'SMSGWAPI:E:10001',
                'Http method \'{0}\' not allowed.'.format(http_method))
        result = urllib2.urlopen(req).read()
        if self.logger:
            self.logger(self.handler_name, method, url + '\n' + str(self.params),
                        result)
        return self._response_parser(result)

    def _is_valid(self, data):
        """Validate response data
        :param data:
        :return:
        """
        error_code, error_message = self.handle_error(data)
        if error_code is not None:
            raise self.GatewayError(error_code, error_message)

    def _response_parser(self, result):
        """Response parser
        :param data:
        :return:
        """
        self.get_format()
        response = None
        if self.response_format == 'PLAIN-TEXT':
            response = result
        elif self.response_format == 'XML':
            response = self.xml_parser(result)
        elif self.response_format == 'JSON':
            response = json.loads(result)
        else:
            raise self.WrongResponseFormat(
                'SMSGWAPI:E:10003',
                'Wrong response format: {0}'.format(self.response_format))
        self._is_valid(response)
        return response

    def get_format(self):
        """Return response format
        :param format:
        :return:
        """
        return self.response_format

    def set_format(self, fmt):
        """Set response format
        :param format:
        :return:
        """
        self.response_format = fmt

    def handle_error(self, data):
        """Handle an error from Gateway
        :param data:
        :return:
        """
        raise NotImplementedError()

    def xml_parser(self, data):
        """GW response XML-parser
        :param data:
        :return:
        """
        raise NotImplementedError()

    def set_headers(self, headers):
        self.headers = headers

    def request(self, method, **kwargs):
        """Request not implemented method
        :param method:
        :param kwargs:
        :return:
        """
        return self._call(method, kwargs)

    def send(self, **kwargs):
        """Sends the SMS-message through the SMS-gateway API
        :param phones_list: a list of phones
        :param message: a message
        :return:
        """
        return self._call('send', kwargs)

    def status(self, **kwargs):
        """Checks the status of the sent message
        :param transaction_id:
        :return:
        """
        return self._call('status', kwargs)

    class HttpMethodNotAllowed(BaseException):
        pass

    class MethodNotFound(BaseException):
        pass

    class WrongResponseFormat(BaseException):
        pass

    class GatewayError(BaseException):
        pass
