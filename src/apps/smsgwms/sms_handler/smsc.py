from .base import SMSHandler


__author__ = "Andrey Lukyanov"
__email__ = "laandrey@gmail.com"
__all__ = ['SMSGWClient']


class SMSGWClient(SMSHandler):
    handler_name = 'smsc'
    methods = {
        'send': ('http://smsc.ru/sys/send.php', 'GET'),
        'status': ('http://smsc.ru/sys/status.php', 'GET'),
        'balance': ('http://smsc.ru/sys/balance.php', 'GET')
    }

    def get_format(self):
        fmt = int(self.params.get('fmt')) if 'fmt' in self.params else None
        if fmt == 2:
            self.set_format('XML')
        elif fmt == 3:
            self.set_format('JSON')

    def handle_error(self, data):
        code, message = None, None
        if self.response_format == 'XML':
            pass
        elif self.response_format == 'JSON' and 'error' in data:
            code, message = data['error'], data['error_code']
        elif self.response_format == 'PLAIN-TEXT' and data.find('ERROR') >= 0:
            code, message = data.split(' = ')[1].split(' ', 1)
        return code, message




