from .base import SMSHandler


__author__ = "Andrey Lukyanov"
__email__ = "laandrey@gmail.com"
__all__ = ['SMSGWClient']


class SMSGWClient(SMSHandler):
    handler_name = 'websms'
    methods = {
        'send': ('http://cab.websms.ru/json_in5.asp', 'GET'),
    }

    def get_format(self):
        if self.response_format != 'JSON':
            self.set_format('JSON')

    def handle_error(self, data):
        code, message = None, None
        if self.response_format == 'XML':
            pass
        elif self.response_format == 'JSON':
            pass
        elif self.response_format == 'PLAIN-TEXT':
            pass
        return code, message
