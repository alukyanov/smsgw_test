from django.conf.urls import patterns, url


smsgw_urlpatterns = patterns(
    'src.apps.smsgwms.views',
    url(r'^(?P<handler_name>\w+)/(?P<method>\w+)$', 'smsgw_view'),
)
