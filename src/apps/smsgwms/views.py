from django.views.generic.base import View
from django.http import JsonResponse

from sms_handler import get_handler
from .models import SMSGWLog


class SMSGWHandlerView(View):
    @property
    def _sms_handler(self):
        return get_handler(self.kwargs['handler_name'].lower())

    def _get_handler_result(self):
        params = {k: v for k, v in self.request.GET.items()}
        params.update({k: v for k, v in self.request.POST.items()})
        method = self.kwargs['method']
        gw = self._sms_handler()
        gw.logger = SMSGWLog.objects.log
        error_code = None
        error_message = None
        status_code = 200
        if self.request.method != gw.methods[method][1]:
            error_code = 405
            error_message = 'Method \'{0}\' not allowed'.format(
                self.request.method)
            status_code = 405

        result = None
        try:
            result = getattr(gw, method)(**params)
        except Exception, e:
            error_code = getattr(e, 'code', 500)
            error_message = e.message
            status_code = 500
        if error_code:
            return JsonResponse({'status': 'error', 'error_code': error_code,
                                 'error_msg': error_message},
                                status=status_code)
        return JsonResponse({'status': 'OK', 'result': result})

    def get(self, request, *args, **kwargs):
        return self._get_handler_result()

    def post(self, request, *args, **kwargs):
        return self._get_handler_result()

    def put(self, request, *args, **kwargs):
        return self._get_handler_result()

    def delete(self, request, *args, **kwargs):
        return self._get_handler_result()
smsgw_view = SMSGWHandlerView.as_view()