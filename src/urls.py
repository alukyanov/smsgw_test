from django.conf.urls import include, url
from apps.smsgwms.urls import smsgw_urlpatterns


urlpatterns = [
    url(r'^SMSGWAPI/', include(smsgw_urlpatterns, namespace='smsgw')),
]
